import logo from './logo.svg';
import './App.css';
import Animal from './Components/Animal';

function App() {
  return (
    <div >
      <Animal kind="cat"></Animal>
    </div>
  );
}

export default App;
